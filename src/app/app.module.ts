import { BrowserModule } from '@angular/platform-browser';
import { NgModule , NO_ERRORS_SCHEMA} from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HomeComponent } from './home/home.component';
import { PropertyDetailComponent } from './property-detail/property-detail.component';
import { LlinePipe } from './lline.pipe';
import { BuyComponent } from './buy/buy.component';
import { AgentdetailComponent } from './agentdetail/agentdetail.component';
import { RentComponent } from './rent/rent.component';
import { AgentlistComponent } from './agentlist/agentlist.component';
import { AgentsComponent } from './agents/agents.component';
import { CommercialRentComponent } from './commercial-rent/commercial-rent.component';
import { CompaniesComponent } from './companies/companies.component';
import { CompanieslistComponent } from './companieslist/companieslist.component';
import { CompanydetailComponent } from './companydetail/companydetail.component';

@NgModule({
  schemas: [ NO_ERRORS_SCHEMA ],
  declarations: [
    AppComponent,
    HomeComponent,
    PropertyDetailComponent,
    LlinePipe,
    BuyComponent,
    AgentdetailComponent,
    RentComponent,
    AgentlistComponent,
    AgentsComponent,
    CommercialRentComponent,
    CompaniesComponent,
    CompanieslistComponent,
    CompanydetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
