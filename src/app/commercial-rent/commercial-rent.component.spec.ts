import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialRentComponent } from './commercial-rent.component';

describe('CommercialRentComponent', () => {
  let component: CommercialRentComponent;
  let fixture: ComponentFixture<CommercialRentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialRentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialRentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
