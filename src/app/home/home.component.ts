import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  private apiUrl= 'http://138.197.103.4/propfin.com/api/v1/companies';
  data: any = {};
  constructor(private http: Http) {
  console.log('why');
    this.getContacts();
    this.getData();
  }
  getData() {
  return this.http.get(this.apiUrl).map((res: Response) => res.json());
  }
  getContacts() {
  this.getData().subscribe(data => {console.log(data); this.data = data; });
  }

}
