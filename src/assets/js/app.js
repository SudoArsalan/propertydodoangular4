
    $(function() {
        $('#ms').change(function() {
            // console.log($(this).val());
        }).multipleSelect({
            width: '100%'
        });
    });




$(function () {
        var availableTags = [
          "ActionScript",
          "AppleScript",
          "Asp",
          "BASIC",
          "C",
          "C++",
          "Clojure",
          "COBOL",
          "ColdFusion",
          "Erlang",
          "Fortran",
          "Groovy",
          "Haskell",
          "Java",
          "JavaScript",
          "Lisp",
          "Perl",
          "PHP",
          "Python",
          "Ruby",
          "Scala",
          "Scheme"
        ];
        $("#inputtags").autocomplete({
            source: availableTags,
            focus: function (event, ui) {
                event.preventDefault();
                $("#inputtags").val(ui.item.value);
            },
            select: function (event, ui) {
                event.preventDefault();
                $( "#alerttext" ).append('<span class="autocomplete-keyword"><span class="autocomplete-result">'+ui.item.value+'</span><span class="autocomplete-remove" data-id="1" data-path="1"><svg class="svg-i i-times-circle" viewBox="0 0 1792 1792"><path d="M1277 1122q0-26-19-45l-181-181 181-181q19-19 19-45 0-27-19-46l-90-90q-19-19-46-19-26 0-45 19l-181 181-181-181q-19-19-45-19-27 0-46 19l-90 90q-19 19-19 46 0 26 19 45l181 181-181 181q-19 19-19 45 0 27 19 46l90 90q19 19 46 19 26 0 45-19l181-181 181 181q19 19 45 19 27 0 46-19l90-90q19-19 19-46zm387-226q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z"></path></svg></span></span>');
                var x = document.getElementById("alertemail");
                x.className = "email-alert-link js-email-alert";
                var y = document.getElementById("l");
                y.value += ui.item.value;
                $("#inputtags").val(ui.item.value);
            }
        });
    });



    $('#category').on('change', function(e){
    		console.log(e.target.value);
    		var id = e.target.value;
        var x1 = document.getElementById("property_type");
        var x2 = document.getElementById("property_period");
        var x3 = document.getElementById("property_price");
        var x4 = document.getElementById("property_bed");
        var x5 = document.getElementById("property_area");
        var x6 = document.getElementById("property_furnish");
        var x7 = document.getElementById("property_amenities");
        var x8 = document.getElementById("property_keyword");

        if (id == 1) {

          x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
          x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
          x6.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
          x7.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
          x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";

          $('#property_type_value').on('change', function(e){
          		console.log(e.target.value);
          		var tid = e.target.value;
              if (tid == 1) {
                x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
                x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
                x6.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
                x7.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
                x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
              }
              else if (tid == 2) {
                x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
                x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
                x6.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
                x7.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
                x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
              }
              else if (tid == 3) {
                x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
                x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
                x6.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
                x7.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
                x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
              }
              else {
                x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
                x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
                x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
                x6.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
                x7.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
                x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
              }
          });
        }
        else if (id == 2) {
          x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
          x2.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x4.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
          x6.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
          x7.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
          x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
        }
        else if (id == 3) {
          x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
          x2.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x4.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
          x6.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
          x7.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
          x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
        }
        else if (id == 4) {
          x1.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push";
          x2.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x3.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x4.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push collapsed";
          x5.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push area collapsed";
          x6.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push furnishings collapsed";
          x7.className = "hidden pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
          x8.className = "pure-control-group pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-4 pure-push search-keyword collapsed";
        }
    });

// response index nav menu
  function menuFunction() {
      var x = document.getElementById("menu_responsive");
      var y = document.getElementById("icon_responsive");
      if (x.className === "module-user-box home portal") {
          x.className += " offcanvas-active";
          y.className += " a-x";
      } else {
          x.className = "module-user-box home portal";
          y.className = "hamburger-icon hide-on-print";
      }
  }
// user index nav bar
  function userBoxFunction() {
      var x = document.getElementById("menu_responsive");
      if (x.className === "module-user-box home portal") {
          x.className += " user-box-active";
      } else {
          x.className = "module-user-box home portal";
      }
  }

// response buy nav menu
  function menuBuyFunction() {
      var x = document.getElementById("menu_responsive");
      var y = document.getElementById("icon_responsive");
      if (x.className === "module-user-box serp property property-landing") {
          x.className += " offcanvas-active";
          y.className += " a-x";
      } else {
          x.className = "module-user-box serp property property-landing";
          y.className = "hamburger-icon hide-on-print";
      }
  }
// user buy nav bar
  function userBuyBoxFunction() {
      var x = document.getElementById("menu_responsive");
      if (x.className === "module-user-box serp property property-landing") {
          x.className += " user-box-active";
      } else {
          x.className = "module-user-box serp property property-landing";
      }
  }

// more detail
  function moreOptionFunction() {
      var x = document.getElementById("more_options");
      var y = document.getElementById("less_options");
      var z = document.getElementById("setting_options");
      x.className = "js-expand-search hidden";
      y.className = "js-expand-search";
      z.className = "settings";
  }
  function lessOptionFunction() {
      var x = document.getElementById("more_options");
      var y = document.getElementById("less_options");
      var z = document.getElementById("setting_options");
      x.className = "js-expand-search";
      y.className = "js-expand-search hidden";
      z.className = "settings minimized";
  }



// user detail

function UserSearchedFunction() {
    var a = document.getElementById("tabsearched");
    var b = document.getElementById("tabsaved");
    var c = document.getElementById("tabcontacted");
    a.className = "selected";
    b.className = "";
    c.className = "";

    var x = document.getElementById("tab-searched");
    var y = document.getElementById("tab-saved");
    var z = document.getElementById("tab-contacted");
    x.className = "tab listing searched selected";
    y.className = "tab listing saved";
    z.className = "tab listing contacted pixel-published";
}

function UserSavedFunction() {
    var a = document.getElementById("tabsearched");
    var b = document.getElementById("tabsaved");
    var c = document.getElementById("tabcontacted");
    a.className = "";
    b.className = "selected";
    c.className = "";

    var x = document.getElementById("tab-searched");
    var y = document.getElementById("tab-saved");
    var z = document.getElementById("tab-contacted");
    x.className = "tab listing searched";
    y.className = "tab listing saved selected";
    z.className = "tab listing contacted pixel-published";
}

function UserContactedFunction() {
    var a = document.getElementById("tabsearched");
    var b = document.getElementById("tabsaved");
    var c = document.getElementById("tabcontacted");
    a.className = "";
    b.className = "";
    c.className = "selected";

    var x = document.getElementById("tab-searched");
    var y = document.getElementById("tab-saved");
    var z = document.getElementById("tab-contacted");
    x.className = "tab listing searched";
    y.className = "tab listing saved";
    z.className = "tab listing contacted pixel-published selected";
}

//

  $("#nav1 li.more").hover(function(){
    $("#nav1 li").removeClass(" is-active");
    $(this).addClass("more is-active");
    return false;
  },
  function (){
      $("#nav1 li.more").removeClass(" is-active");
    }
  );


// Buy

// more detail
  function morePropertyFunction() {
      var x = document.getElementById("extras_options");
      var show = document.getElementById("optionShow");
      var hide = document.getElementById("optionHide");

      var hide1 = document.getElementById("hide1");
      var hide2 = document.getElementById("hide2");
      var hide3 = document.getElementById("hide3");

      if (x.className == "js-show-extras open") {
        x.className = "js-show-extras";
        show.className = "hide";
        hide.className = "show";

        hide1.className = "hide pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
        hide2.className = "hide pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
        hide3.className = "hide pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
      }else {
        x.className = "js-show-extras open";
        show.className = "show";
        hide.className = "hide";

        hide1.className = "pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
        hide2.className = "pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
        hide3.className = "pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pure-push";
      }
  }



  // Call detail
    function calldetailFunction() {
        var x = document.getElementById("calldetail");
        var y = document.getElementById("numdetail");
        x.className = "hide print";
        y.className = "button button-phone reveal hide-on-print js-tooltipster-call js-userbox-call tooltipstered pixel-published gtm-call-sent revealed actions-added";
    }

  // Call detail
    function emaildetailFunction() {
        var x = document.getElementById("contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form open";
        y.className = "country-ae js popup-open";
    }

    function closeemaildetailFunction() {
        var x = document.getElementById("contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form";
        y.className = "country-ae js";
    }

    function agent_emaildetailFunction() {
        var x = document.getElementById("agent-contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form open";
        y.className = "country-ae js popup-open";
    }

    function agent_closeemaildetailFunction() {
        var x = document.getElementById("agent-contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form";
        y.className = "country-ae js";
    }


    function brocker_emaildetailFunction() {
        var x = document.getElementById("brocker-contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form open";
        y.className = "country-ae js popup-open";
    }

    function broker_closeemaildetailFunction() {
        var x = document.getElementById("brocker-contact-form-popup");
        var y = document.getElementById("html");
        x.className = "popup contact-form";
        y.className = "country-ae js";
    }

  // Call detail
    function viewMapFunction() {
        var x = document.getElementById("view_map");
        var y = document.getElementById("map_hide");
        var z = document.getElementById("map_show");
        var map = document.getElementById("serp-map");
        if (x.className == "button map-button view-map") {
          x.className = "button map-button view-list";
          y.className = "";
          z.className = "hide";
          map.className = "";
        }
        else {
          x.className = "button map-button view-map";
          y.className = "hide";
          z.className = "";
          map.className = "hide";
        }
    }


    $(function () {
            var availableTags = [
              "Abu Duabi",
              "Al Reem Island",
              "Arabian Ranches",
              "Shams Abu Dhabi",
              "Al Furjan",
              "Masakin Al Furjan"
            ];
            $("#locationinputtags").autocomplete({
                source: availableTags,
                focus: function (event, ui) {
                    event.preventDefault();
                    $("#inputtags").val(ui.item.value);
                },
                select: function (event, ui) {
                    event.preventDefault();
                    var x = document.getElementById("closesearch");
                    x.className = "close-btn";
                    $("#locationinputtags").val(ui.item.value);
                }
            });
        });


        function closesearch() {
          var x = document.getElementById("closesearch");
          x.className = "close-btn hidden";
          $("#locationinputtags").val("");
        }


// Tab About


   var change_funT1 = document.getElementById('aboutMe_tab');
   var change_funT2 = document.getElementById('properties_tab');
   var change_funT3 = document.getElementById('transactions_tab');
   var change_funD1 = document.getElementById('aboutMe_data');
   var change_funD2 = document.getElementById('properties_data');
   var change_funD3 = document.getElementById('transactions_data');
   function change_fun1() {
     change_funT1.className = "tab-button active";
     change_funT2.className = "tab-button";
     change_funT3.className = "tab-button";
     change_funD1.className = "user-tab user-tab-about-me active";
     change_funD2.className = "user-tab user-tab-properties hidden";
     change_funD3.className = "user-tab user-tab-transactions hidden";
   }
   function change_fun2() {
     change_funT1.className = "tab-button";
     change_funT2.className = "tab-button active";
     change_funT3.className = "tab-button";
     change_funD1.className = "user-tab user-tab-about-me hidden";
     change_funD2.className = "user-tab user-tab-properties active";
     change_funD3.className = "user-tab user-tab-transactions hidden";
   }
   function change_fun3() {
     change_funT1.className = "tab-button";
     change_funT2.className = "tab-button";
     change_funT3.className = "tab-button active";
     change_funD1.className = "user-tab user-tab-about-me hidden";
     change_funD2.className = "user-tab user-tab-properties hidden";
     change_funD3.className = "user-tab user-tab-transactions active";
   }

// agent detail
// Call detail
  function agent_calldetailFunction() {
      var x = document.getElementById("calldetail");
      var y = document.getElementById("numdetail");
      x.className += " hidden";
      y.className = "action-button call-agent reveal pixel-published gtm-call-sent revealed";
  }

  // property Call detail
    function property_calldetailFunction() {
        var x = document.getElementById("property_calldetail");
        var y = document.getElementById("property_numdetail");
        x.className += " hidden";
        y.className = "button button-phone reveal hide-on-print js-tooltipster-call js-userbox-call";
    }


    // Company detail
    // agent Call detail
      function agentD_calldetailFunction() {
          var x = document.getElementById("agent_calldetail");
          var y = document.getElementById("agent_numdetail");
          x.className += " hidden";
          y.className = "button button-phone reveal hide-on-print js-tooltipster-call js-userbox-call";
      }
